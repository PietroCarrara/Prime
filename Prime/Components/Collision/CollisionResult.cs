using Prime;
using Microsoft.Xna.Framework;

namespace Prime
{
	public class CollisionResult
	{
		public Direction Direction;
	}

	public enum Direction
	{
		Up,
		Down,
		Left,
		Right
	}
}
